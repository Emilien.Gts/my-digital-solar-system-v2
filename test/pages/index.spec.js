import { mount } from '@vue/test-utils'
import index from './../../pages/index.vue'

describe('index', () => {
  test('testing index', () => {
    const wrapper = mount(index, {
      stubs: {
        NuxtLink: true,
        BJumbotron: true,
        BButton: true
      },
    })
    expect(wrapper.vm).toBeTruthy()
  })
})