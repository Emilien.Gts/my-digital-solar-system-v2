import { mount } from '@vue/test-utils'
import Badge from './../../components/Badge/Badge.vue'

describe('Badge', () => {
  test('testing Badge', () => {
    const wrapper = mount(Badge, {
      stubs: {
        NuxtLink: true,
        BButton: true,
        BBadge: true
      }
    })
    expect(wrapper.vm).toBeTruthy()
  })
})