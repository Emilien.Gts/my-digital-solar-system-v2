<!-- NAME -->

## Name

My Digital Solar System - NuxtJS

<!-- DESCRIPTION -->

## Description

This project is a web application that will allow you to discover all the system solar thanks to API : [Le Système Solaire](https://api.le-systeme-solaire.net/). This project has been implemented using the [NuxtJS](https://fr.nuxtjs.org/) Framework.

<!-- CONTEXT -->

## Context

This project is a school project that will allow me to evaluate my skills concerning Javascript, and especially the [NuxtJS](https://fr.nuxtjs.org/) Framework. The expected outcome was as follows:

_List of stars_

A page listing all the stars should be available with the following features:

- Filter on criteria such as whether it is a planet (isPlanet) or not
- Filter on having moons

_Star page_

A page displaying a star : :

- All the information related to the on-call service must be displayed on this page.
- If the star is a planet, with moons, quick access to its moons must be available.

_Favorites page_

It should be possible to specify stars as favorites and remove them from the favorites list. When you add a star as a favorite, a notification should appear to let you know.
The display of stars on this page reuses the list display of the 'list of stars' page.

<!-- INSTALLATION -->

## Installation

To begin, you need to install the project's dependencies using the following command:

    yarn install

To do this, you need to install [yarn](https://yarnpkg.com/) (or you can use [npm](https://www.npmjs.com/))

<!-- USAGE -->

## Usage

You can start the application using the following command:

    yarn dev

Or is npm equivalent.

<!-- VISUALS -->

## Visuals

### Homepage

<img src="https://gitlab.com/Emilien.Gts/my-digital-solar-system-v2/-/raw/master/assets/homepage.png" width="350px">

### List of stars

<img src="https://gitlab.com/Emilien.Gts/my-digital-solar-system-v2/-/raw/master/assets/list_of_stars.png" width="350px">

### Detail of a star

<img src="https://gitlab.com/Emilien.Gts/my-digital-solar-system-v2/-/raw/master/assets/detail_of_a_star.png" width="350px">

### Favorites

<img src="https://gitlab.com/Emilien.Gts/my-digital-solar-system-v2/-/raw/master/assets/favorites.png" width="350px">

<!-- LICENSE -->

## License

[License](https://gitlab.com/Emilien.Gts/my-digital-solar-system-v2/-/blob/master/LICENSE)

<!-- PROJECT STATUS -->

## Project Status

This project is finished. The final mark is 14.5/20.
