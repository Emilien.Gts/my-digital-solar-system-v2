export const state = () => ({
  list: ['terre', 'mars', 'lune', 'jupiter', 'ananke', 'leda', 'adrastee', 'megaclite', 'erinome']
})

export const mutations = {
  add(state, favorite) {
    state.list.push(favorite)
  },
  delete(state, favorite) {
    let i = '';
    state.list.map((f, index) => {
      f === favorite && (i = index);
    });

    i !== '' && state.list.splice(i, 1);
  }
}

export const getters = {
  get(state) {
    return state.list
  }
}