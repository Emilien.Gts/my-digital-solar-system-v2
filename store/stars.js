export const state = () => ({
  list: []
})

export const mutations = {
  set(state, stars) {
    state.list = stars;
  }
}

export const getters = {
  get(state) {
    return state.list
  }
}